import datetime
import time

from selenium import webdriver
from selenium.common import StaleElementReferenceException

from src.helper import config_wd, wait_by_xpath, ts
#
from dotenv import load_dotenv
from pathlib import Path
#
import re
SH = Path(__file__).parent
load_dotenv('../_env/.env')

from fixture.I import I


def open_(wd):
  wd.get('https://www.mihong.vn/vi/gia-vang-trong-nuoc')  # for some reason, we must open this url to get below code run-able  #TODO why cannot run if not refresh at this url?


def parse_date(date_string):
    day = int(date_string[0:2])
    month = int(date_string[2:4])
    year = int(date_string[4:8])
    date_object = datetime.date(year, month, day)
    return date_object


def click_option(wd, xpath):
  attempts = 0
  while attempts < 3:
    try:
      element = wait_by_xpath(wd,xpath)
      element.click()
      break
    except StaleElementReferenceException:
      attempts += 1
      if attempts == 3:
          raise


def get_text_with_retry(wd, xpath, retries=3):
    for _ in range(retries):
        try:
            element = wait_by_xpath(wd,xpath)
            return element.text
        except StaleElementReferenceException:
            continue
    raise Exception(f"Failed to retrieve element text after {retries} attempts")


try:
  wd = webdriver.Chrome() ; config_wd(wd)
  open_(wd)

  data_list=[]
  wait_by_xpath(wd, '//*[@class="control next"]').click()
  #        ddmmyyyy
  start = '01012023'
  end   = '01012024'

  start_date = parse_date(start)
  end_date = parse_date(end)
  current_date = start_date
  while current_date <= end_date:
    day = current_date.day
    month = current_date.month
    year = current_date.year
    'send day'; click_option(wd, f'//*[@name="cond1-day"]/option[text()[contains(.,"{day:02d}")]]')
    'send month'; click_option(wd, f'//*[@name="cond1-month"]/option[text()[contains(.,"{month:02d}")]]')
    'send year'; click_option(wd, f'//*[@name="cond1-year"]/option[text()[contains(.,"{year}")]]')


    buying_price = get_text_with_retry(wd, f'(//*[@class="price-number"])[17]')
    selling_price = get_text_with_retry(wd, f'(//*[@class="price-number"])[18]')

    data_list.append({
      "date": f'{day:02d}/{month:02d}/{year}',
      "buying_price": buying_price,
      "selling_price": selling_price,
    })

    current_date += datetime.timedelta(days=1)
    debug = 00

finally:  wd.quit()