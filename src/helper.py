"""
ref. https://pypi.org/project/webdriver-manager/
"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
#
from dotenv import load_dotenv
import os
from pathlib import Path
import time
from datetime import datetime
import re


SH = Path(__file__).parent

##region TODO mv to :GH/_shared_helper/

#region wd util
WD_IMPLICITLY_WAIT=12
WDW_TIMEOUT=12  # WDW_xx aka WebDriverWait_xx

def ts():
  return datetime.now().strftime("%Y%m%d_%H%M%S")

def config_wd(wd):
  wd.implicitly_wait(WD_IMPLICITLY_WAIT)
  wd.maximize_window()


def wait_by_xpath(wd:'webdriver', xpath, timeout=None):
  wdw = WebDriverWait(wd, timeout=timeout if timeout else WDW_TIMEOUT)  # wdw aka wd_wait
  e = wdw.until(EC.visibility_of_element_located((By.XPATH, xpath)))  # e aka element

  time.sleep( int(os.environ.get('SLOW_DOWN',0)) )

  return e


def wait_by_css(wd:'webdriver', css, timeout=None):
  wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT if timeout is None else timeout)  # wdw aka wd_wait
  e = wdw.until(EC.visibility_of_element_located((By.CSS_SELECTOR, css)))  # e aka element

  time.sleep( int(os.environ.get('SLOW_DOWN',0)) )

  return e



def waitall_by_xpath(wd:'webdriver', xpath, timeout=None):
  wdw = WebDriverWait(wd, timeout=timeout if timeout else WDW_TIMEOUT)  # wdw aka wd_wait
  e_list = wdw.until(EC.visibility_of_all_elements_located((By.XPATH, xpath)))  # e aka element

  time.sleep( int(os.environ.get('SLOW_DOWN',0)) )

  return e_list

#endregion wd util




def login_ztna_tenant(wd, I):
  '';    admin_portal_url='https://ztna.secureage.com/#/portal_login'
  wd.get(admin_portal_url)

  'enter tenant'
  wait_by_xpath(wd, '//input[@aria-label="Tenant Name"]').send_keys(I.tenant)
  wait_by_xpath(wd, '//button//*[text()="Next"]').click()

  'login w/ ms acc'
  wait_by_xpath(wd, '//button//*[text()="Sign in with Microsoft"]').click()

  wait_by_xpath(wd, '//*[@placeholder="Email, phone, or Skype"]').send_keys(I.ms_login_email)
  wait_by_xpath(wd, '//input[@value="Next"]').click()

  wait_by_xpath(wd, '//*[@placeholder="Password"]').send_keys(I.ms_login_passw)
  wait_by_xpath(wd, '//input[@value="Sign in"]').click()

  wait_by_xpath(wd, '//*[text()="Stay signed in?"]')
  wait_by_xpath(wd, '//input[@value="Yes"]').click()

